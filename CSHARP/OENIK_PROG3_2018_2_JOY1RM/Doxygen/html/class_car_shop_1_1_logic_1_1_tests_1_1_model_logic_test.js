var class_car_shop_1_1_logic_1_1_tests_1_1_model_logic_test =
[
    [ "Setup", "class_car_shop_1_1_logic_1_1_tests_1_1_model_logic_test.html#ac161b8f54565b8339b787b02edf56b83", null ],
    [ "WhenDeleteAModel_TheDeleteFunctionIsCalled", "class_car_shop_1_1_logic_1_1_tests_1_1_model_logic_test.html#a615557abdacdb0d027a0b8d659378da0", null ],
    [ "WhenGetAllTheModels_ReturnAllTheThreeModels", "class_car_shop_1_1_logic_1_1_tests_1_1_model_logic_test.html#ab28ca200cf2d06dd9d00f990789f7937", null ],
    [ "WhenInsertAModel_TheInsertFunctionIsCalled", "class_car_shop_1_1_logic_1_1_tests_1_1_model_logic_test.html#a8d13c29ccc85dd8aeb8e57f4c7d2cfdf", null ],
    [ "WhenUpdateAModel_TheUpdateFunctionIsCalled", "class_car_shop_1_1_logic_1_1_tests_1_1_model_logic_test.html#a0ac117a5f406478e024d414e78b7e5b1", null ]
];