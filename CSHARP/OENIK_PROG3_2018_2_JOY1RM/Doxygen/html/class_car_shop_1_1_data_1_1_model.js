var class_car_shop_1_1_data_1_1_model =
[
    [ "Model", "class_car_shop_1_1_data_1_1_model.html#af81b23077c51e4ed4de428a7a463ee52", null ],
    [ "Brand", "class_car_shop_1_1_data_1_1_model.html#add21c73639db2e06006b88fb3f8e13ef", null ],
    [ "BrandId", "class_car_shop_1_1_data_1_1_model.html#a577d917874b75bda6fc433947c0ad73c", null ],
    [ "EngineSize", "class_car_shop_1_1_data_1_1_model.html#a866496b3f054d01c5ab6e5881e54d914", null ],
    [ "HorsePower", "class_car_shop_1_1_data_1_1_model.html#a16eb33d6d66bbc956c79024ddcebfda0", null ],
    [ "Id", "class_car_shop_1_1_data_1_1_model.html#a6786cd3f07289dc3ebbf477299e9ab49", null ],
    [ "ModelExtras", "class_car_shop_1_1_data_1_1_model.html#ade754a2dd663250ba2a0ea031f25686a", null ],
    [ "ModelName", "class_car_shop_1_1_data_1_1_model.html#ab308b08389da477ceb8f490c2adc8838", null ],
    [ "Price", "class_car_shop_1_1_data_1_1_model.html#a3a63db324db2926e2cc9eedb11050d62", null ],
    [ "ReleaseYear", "class_car_shop_1_1_data_1_1_model.html#a0c47f9680b2d49cc97c179da64ece1a7", null ]
];