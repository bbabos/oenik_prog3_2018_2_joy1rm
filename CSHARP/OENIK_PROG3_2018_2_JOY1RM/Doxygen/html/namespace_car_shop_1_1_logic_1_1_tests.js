var namespace_car_shop_1_1_logic_1_1_tests =
[
    [ "BrandLogicTest", "class_car_shop_1_1_logic_1_1_tests_1_1_brand_logic_test.html", "class_car_shop_1_1_logic_1_1_tests_1_1_brand_logic_test" ],
    [ "ExtraLogicTest", "class_car_shop_1_1_logic_1_1_tests_1_1_extra_logic_test.html", "class_car_shop_1_1_logic_1_1_tests_1_1_extra_logic_test" ],
    [ "ModelExtrasLogicTest", "class_car_shop_1_1_logic_1_1_tests_1_1_model_extras_logic_test.html", "class_car_shop_1_1_logic_1_1_tests_1_1_model_extras_logic_test" ],
    [ "ModelLogicTest", "class_car_shop_1_1_logic_1_1_tests_1_1_model_logic_test.html", "class_car_shop_1_1_logic_1_1_tests_1_1_model_logic_test" ],
    [ "NotCrudLogicTest", "class_car_shop_1_1_logic_1_1_tests_1_1_not_crud_logic_test.html", "class_car_shop_1_1_logic_1_1_tests_1_1_not_crud_logic_test" ]
];