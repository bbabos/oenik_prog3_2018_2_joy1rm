var class_car_shop_1_1_logic_1_1_tests_1_1_not_crud_logic_test =
[
    [ "Setup", "class_car_shop_1_1_logic_1_1_tests_1_1_not_crud_logic_test.html#af5037ff529e1d2ed3f843ab616b3fc9b", null ],
    [ "WhenGetAveragePriceForBrands_ReturnsTheCorrectNumbers", "class_car_shop_1_1_logic_1_1_tests_1_1_not_crud_logic_test.html#ab12b5e8725adc70ef23cb9c4deec7b05", null ],
    [ "WhenGetCarsWithSumPrice_ReturnsTheCorrectNumbers", "class_car_shop_1_1_logic_1_1_tests_1_1_not_crud_logic_test.html#a541cd43ad00d617fe61edf72116bbf9b", null ],
    [ "WhenGetNumberExtraCategoryUseage_ReturnTheCorrectNumbers", "class_car_shop_1_1_logic_1_1_tests_1_1_not_crud_logic_test.html#ac48ca904f58aa2f48bb76b86b70d785a", null ]
];