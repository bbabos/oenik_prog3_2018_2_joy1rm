var class_car_shop_1_1_data_1_1_car_shop_d_b_entities =
[
    [ "CarShopDBEntities", "class_car_shop_1_1_data_1_1_car_shop_d_b_entities.html#a6321ac569a3198107400729e92e0816f", null ],
    [ "OnModelCreating", "class_car_shop_1_1_data_1_1_car_shop_d_b_entities.html#a2d2d80de8f560b007083bb889fe6af11", null ],
    [ "Brands", "class_car_shop_1_1_data_1_1_car_shop_d_b_entities.html#a2fcf3c9814bb3b5608c1c1cbe8c94e84", null ],
    [ "Extras", "class_car_shop_1_1_data_1_1_car_shop_d_b_entities.html#a05c7d1643d1174e84129d5d33e681ec9", null ],
    [ "ModelExtras", "class_car_shop_1_1_data_1_1_car_shop_d_b_entities.html#a334f2d507d19a6b737cf63f807591990", null ],
    [ "Models", "class_car_shop_1_1_data_1_1_car_shop_d_b_entities.html#a137b30265f52c75c847dfcb014dd5789", null ]
];