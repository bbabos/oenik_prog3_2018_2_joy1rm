var class_car_shop_1_1_logic_1_1_tests_1_1_model_extras_logic_test =
[
    [ "Setup", "class_car_shop_1_1_logic_1_1_tests_1_1_model_extras_logic_test.html#a3b921f21947f6b74c79f2484bc5fb95c", null ],
    [ "WhenDeleteAModelExtras_TheDeleteFunctionIsCalled", "class_car_shop_1_1_logic_1_1_tests_1_1_model_extras_logic_test.html#ab74aac927cdc3e85ff8ebfe29ad6c2b0", null ],
    [ "WhenGetAllTheModelExtrass_ReturnAllTheThreeModelExtrass", "class_car_shop_1_1_logic_1_1_tests_1_1_model_extras_logic_test.html#af65cbc3e5380d218caf984081a2b53ca", null ],
    [ "WhenInsertAModelExtras_TheInsertFunctionIsCalled", "class_car_shop_1_1_logic_1_1_tests_1_1_model_extras_logic_test.html#a96bdf5292f70e6e72b7913a0540f3e21", null ],
    [ "WhenUpdateAModelExtras_TheUpdateFunctionIsCalled", "class_car_shop_1_1_logic_1_1_tests_1_1_model_extras_logic_test.html#a5ad505e93e09e7253b9ed647bd7d4998", null ]
];