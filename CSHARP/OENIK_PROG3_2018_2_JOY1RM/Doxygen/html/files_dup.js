var files_dup =
[
    [ "CarShop.Data", "dir_d6671d78f862d5c4e9ec294df30502f2.html", "dir_d6671d78f862d5c4e9ec294df30502f2" ],
    [ "CarShop.JavaWeb", "dir_b98a969bad2d46a0da0dbe4e7ce99669.html", "dir_b98a969bad2d46a0da0dbe4e7ce99669" ],
    [ "CarShop.Logic", "dir_4bf0a402d2b2c3c6bea24f0868e0183a.html", "dir_4bf0a402d2b2c3c6bea24f0868e0183a" ],
    [ "CarShop.Logic.Tests", "dir_28441cc2958c97211e664565f14a2042.html", "dir_28441cc2958c97211e664565f14a2042" ],
    [ "CarShop.Program", "dir_a56631e499bf974c3e43572e40046d05.html", "dir_a56631e499bf974c3e43572e40046d05" ],
    [ "CarShop.Repository", "dir_5f6808df2b4d62f7abd70454fc3fecb6.html", "dir_5f6808df2b4d62f7abd70454fc3fecb6" ],
    [ "CarShop.Repository.Tests", "dir_7f9e5d03b869abc726e08d86407086a4.html", "dir_7f9e5d03b869abc726e08d86407086a4" ],
    [ "obj", "dir_43724e81dd40e09f32417973865cdd64.html", "dir_43724e81dd40e09f32417973865cdd64" ]
];