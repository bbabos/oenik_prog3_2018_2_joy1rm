var class_car_shop_1_1_data_1_1_brand =
[
    [ "Brand", "class_car_shop_1_1_data_1_1_brand.html#aa018f36742fd7057c88e165314ef23af", null ],
    [ "BrandName", "class_car_shop_1_1_data_1_1_brand.html#a4f3fc62284dd9ed5f0f72785ef31986b", null ],
    [ "CountryName", "class_car_shop_1_1_data_1_1_brand.html#a5b9a2b8f875b99cc6f568160b02a9d04", null ],
    [ "FoundedIn", "class_car_shop_1_1_data_1_1_brand.html#a966d3773bee99c8d6888969700b54366", null ],
    [ "Id", "class_car_shop_1_1_data_1_1_brand.html#aba4facf5872a4411e821e1eee0da737b", null ],
    [ "Models", "class_car_shop_1_1_data_1_1_brand.html#add2cf9b81e21828c331a23e32d565a10", null ],
    [ "Sales", "class_car_shop_1_1_data_1_1_brand.html#a921122dd3f526d52737bfdc41be67144", null ],
    [ "WebUrl", "class_car_shop_1_1_data_1_1_brand.html#ae33ffb537de3f35102b260e6d3cf0222", null ]
];