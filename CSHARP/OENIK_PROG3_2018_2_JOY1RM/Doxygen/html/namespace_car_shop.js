var namespace_car_shop =
[
    [ "Data", "namespace_car_shop_1_1_data.html", "namespace_car_shop_1_1_data" ],
    [ "JavaWeb", "namespace_car_shop_1_1_java_web.html", "namespace_car_shop_1_1_java_web" ],
    [ "Logic", "namespace_car_shop_1_1_logic.html", "namespace_car_shop_1_1_logic" ],
    [ "Program", "namespace_car_shop_1_1_program.html", "namespace_car_shop_1_1_program" ],
    [ "Repository", "namespace_car_shop_1_1_repository.html", "namespace_car_shop_1_1_repository" ]
];