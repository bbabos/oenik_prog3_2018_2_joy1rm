var class_car_shop_1_1_logic_1_1_tests_1_1_brand_logic_test =
[
    [ "Setup", "class_car_shop_1_1_logic_1_1_tests_1_1_brand_logic_test.html#a25b19e43b2fccbc8ea6fed17fd643248", null ],
    [ "WhenDeleteABrand_TheDeleteFunctionIsCalled", "class_car_shop_1_1_logic_1_1_tests_1_1_brand_logic_test.html#a7aa55c01d8de6ae416aff54dd6b973e4", null ],
    [ "WhenGetAllTheBrands_ReturnAllTheThreeBrands", "class_car_shop_1_1_logic_1_1_tests_1_1_brand_logic_test.html#a998fa5d35a313514f7d9ea11280aa966", null ],
    [ "WhenInsertABrand_TheInsertFunctionIsCalled", "class_car_shop_1_1_logic_1_1_tests_1_1_brand_logic_test.html#a6ba12c1f716f792422b6fc87825dd3df", null ],
    [ "WhenUpdateABrand_TheUpdateFunctionIsCalled", "class_car_shop_1_1_logic_1_1_tests_1_1_brand_logic_test.html#a2f3914b151a2910ec25ceacd0a7b80f9", null ]
];