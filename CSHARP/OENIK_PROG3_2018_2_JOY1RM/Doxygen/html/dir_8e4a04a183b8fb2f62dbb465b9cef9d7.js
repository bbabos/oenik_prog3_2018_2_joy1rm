var dir_8e4a04a183b8fb2f62dbb465b9cef9d7 =
[
    [ "CarShop.Data", "dir_3b38a25d939c748489d5a0c69d5b21a8.html", "dir_3b38a25d939c748489d5a0c69d5b21a8" ],
    [ "CarShop.JavaWeb", "dir_982c97704bce2f71afe7a219d75dc67e.html", "dir_982c97704bce2f71afe7a219d75dc67e" ],
    [ "CarShop.Logic", "dir_8d6a2ae3e073f7eb223619d43740ec81.html", "dir_8d6a2ae3e073f7eb223619d43740ec81" ],
    [ "CarShop.Logic.Tests", "dir_b8eaa8c917b4f1e0f3941afa7cdcae4b.html", "dir_b8eaa8c917b4f1e0f3941afa7cdcae4b" ],
    [ "CarShop.Program", "dir_c518f2a40c54bb4e353a8aa4855edf55.html", "dir_c518f2a40c54bb4e353a8aa4855edf55" ],
    [ "CarShop.Repository", "dir_3bee857cf34ddf2697964cb4a9440e11.html", "dir_3bee857cf34ddf2697964cb4a9440e11" ],
    [ "CarShop.Repository.Tests", "dir_bab00209091c3ecdd1d52b79307ae08c.html", "dir_bab00209091c3ecdd1d52b79307ae08c" ],
    [ "obj", "dir_f002426a6f419520da1f9a6c2c617d48.html", "dir_f002426a6f419520da1f9a6c2c617d48" ]
];