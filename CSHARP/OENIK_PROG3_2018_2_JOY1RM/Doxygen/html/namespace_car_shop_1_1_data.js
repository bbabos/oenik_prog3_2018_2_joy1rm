var namespace_car_shop_1_1_data =
[
    [ "Brand", "class_car_shop_1_1_data_1_1_brand.html", "class_car_shop_1_1_data_1_1_brand" ],
    [ "CarShopDBEntities", "class_car_shop_1_1_data_1_1_car_shop_d_b_entities.html", "class_car_shop_1_1_data_1_1_car_shop_d_b_entities" ],
    [ "Extra", "class_car_shop_1_1_data_1_1_extra.html", "class_car_shop_1_1_data_1_1_extra" ],
    [ "Model", "class_car_shop_1_1_data_1_1_model.html", "class_car_shop_1_1_data_1_1_model" ],
    [ "ModelExtras", "class_car_shop_1_1_data_1_1_model_extras.html", "class_car_shop_1_1_data_1_1_model_extras" ]
];