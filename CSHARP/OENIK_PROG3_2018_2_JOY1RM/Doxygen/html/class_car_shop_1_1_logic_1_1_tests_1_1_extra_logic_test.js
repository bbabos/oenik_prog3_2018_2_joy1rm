var class_car_shop_1_1_logic_1_1_tests_1_1_extra_logic_test =
[
    [ "Setup", "class_car_shop_1_1_logic_1_1_tests_1_1_extra_logic_test.html#ae56bdda6493f3e2638992a8536c04800", null ],
    [ "WhenDeleteAnExtra_TheDeleteFunctionIsCalled", "class_car_shop_1_1_logic_1_1_tests_1_1_extra_logic_test.html#a6c16efd0b75822efccf45ac2f26fc2de", null ],
    [ "WhenGetllTheExtras_ReturnAllTheThreeExtras", "class_car_shop_1_1_logic_1_1_tests_1_1_extra_logic_test.html#ab371441bae7375501747c281e99ce9d1", null ],
    [ "WhenInsertAnExtra_TheInsertFunctionIsCalled", "class_car_shop_1_1_logic_1_1_tests_1_1_extra_logic_test.html#a616eaf419e72f388f0b6cc0d8de3df3a", null ],
    [ "WhenUpdateAnExtra_TheUpdateFunctionIsCalled", "class_car_shop_1_1_logic_1_1_tests_1_1_extra_logic_test.html#a8031dc5a9537e3f859a42a7b59f91022", null ]
];