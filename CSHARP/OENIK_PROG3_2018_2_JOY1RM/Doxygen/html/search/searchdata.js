var indexSectionsWithContent =
{
  0: "bcdeghijlmnprstuw",
  1: "bceijmn",
  2: "c",
  3: "bdegijmnpstuw",
  4: "u",
  5: "behlmpr",
  6: "cln"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "functions",
  4: "variables",
  5: "properties",
  6: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Functions",
  4: "Variables",
  5: "Properties",
  6: "Pages"
};

