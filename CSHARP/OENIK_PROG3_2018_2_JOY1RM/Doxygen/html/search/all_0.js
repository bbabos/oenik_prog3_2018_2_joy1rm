var searchData=
[
  ['brand',['Brand',['../class_car_shop_1_1_data_1_1_brand.html',1,'CarShop::Data']]],
  ['branditem',['BrandItem',['../class_car_shop_1_1_program_1_1_brand_item.html',1,'CarShop.Program.BrandItem'],['../class_car_shop_1_1_program_1_1_brand_item.html#a7a0d4a9ecc201fec968a90233f30fd79',1,'CarShop.Program.BrandItem.BrandItem()']]],
  ['brandlogictest',['BrandLogicTest',['../class_car_shop_1_1_logic_1_1_tests_1_1_brand_logic_test.html',1,'CarShop::Logic::Tests']]],
  ['brandrepo',['BrandRepo',['../class_car_shop_1_1_repository_1_1_brand_repo.html',1,'CarShop.Repository.BrandRepo'],['../class_car_shop_1_1_logic_1_1_car_shop_logic.html#a578638c8383cde67f5d7d6ed8f429532',1,'CarShop.Logic.CarShopLogic.BrandRepo()'],['../interface_car_shop_1_1_logic_1_1_i_logic.html#aabab0828a2ba73aaf2dd4afd22ece310',1,'CarShop.Logic.ILogic.BrandRepo()']]],
  ['brandsubitem',['BrandSubItem',['../class_car_shop_1_1_program_1_1_brand_sub_item.html',1,'CarShop.Program.BrandSubItem'],['../class_car_shop_1_1_program_1_1_brand_sub_item.html#a047b9005b3fbfcd135db2d5f66647996',1,'CarShop.Program.BrandSubItem.BrandSubItem(string title, ILogic logic)'],['../class_car_shop_1_1_program_1_1_brand_sub_item.html#a3a3fe5e8be3177228daef53dca84cb41',1,'CarShop.Program.BrandSubItem.BrandSubItem(string title, ILogic logic, Brand brand)']]]
];
