var hierarchy =
[
    [ "CarShop.Data.Brand", "class_car_shop_1_1_data_1_1_brand.html", null ],
    [ "CarShop.Logic.Tests.BrandLogicTest", "class_car_shop_1_1_logic_1_1_tests_1_1_brand_logic_test.html", null ],
    [ "DbContext", null, [
      [ "CarShop.Data.CarShopDBEntities", "class_car_shop_1_1_data_1_1_car_shop_d_b_entities.html", null ]
    ] ],
    [ "CarShop.Data.Extra", "class_car_shop_1_1_data_1_1_extra.html", null ],
    [ "CarShop.Logic.Tests.ExtraLogicTest", "class_car_shop_1_1_logic_1_1_tests_1_1_extra_logic_test.html", null ],
    [ "CarShop.Logic.ILogic", "interface_car_shop_1_1_logic_1_1_i_logic.html", [
      [ "CarShop.Logic.CarShopLogic", "class_car_shop_1_1_logic_1_1_car_shop_logic.html", null ]
    ] ],
    [ "CarShop.Repository.IRepository< T >", "interface_car_shop_1_1_repository_1_1_i_repository.html", null ],
    [ "CarShop.Repository.IRepository< Brand >", "interface_car_shop_1_1_repository_1_1_i_repository.html", [
      [ "CarShop.Repository.BrandRepo", "class_car_shop_1_1_repository_1_1_brand_repo.html", null ]
    ] ],
    [ "CarShop.Repository.IRepository< Extra >", "interface_car_shop_1_1_repository_1_1_i_repository.html", [
      [ "CarShop.Repository.ExtraRepo", "class_car_shop_1_1_repository_1_1_extra_repo.html", null ]
    ] ],
    [ "CarShop.Repository.IRepository< Model >", "interface_car_shop_1_1_repository_1_1_i_repository.html", [
      [ "CarShop.Repository.ModelRepo", "class_car_shop_1_1_repository_1_1_model_repo.html", null ]
    ] ],
    [ "CarShop.Repository.IRepository< ModelExtras >", "interface_car_shop_1_1_repository_1_1_i_repository.html", [
      [ "CarShop.Repository.ModelExtrasRepo", "class_car_shop_1_1_repository_1_1_model_extras_repo.html", null ]
    ] ],
    [ "CarShop.Program.Item", "class_car_shop_1_1_program_1_1_item.html", [
      [ "CarShop.Program.BrandItem", "class_car_shop_1_1_program_1_1_brand_item.html", null ],
      [ "CarShop.Program.BrandSubItem", "class_car_shop_1_1_program_1_1_brand_sub_item.html", null ],
      [ "CarShop.Program.ExtrasItem", "class_car_shop_1_1_program_1_1_extras_item.html", null ],
      [ "CarShop.Program.ExtrasSubItem", "class_car_shop_1_1_program_1_1_extras_sub_item.html", null ],
      [ "CarShop.Program.JavaItem", "class_car_shop_1_1_program_1_1_java_item.html", null ],
      [ "CarShop.Program.Menu", "class_car_shop_1_1_program_1_1_menu.html", null ],
      [ "CarShop.Program.ModelExtrasItem", "class_car_shop_1_1_program_1_1_model_extras_item.html", null ],
      [ "CarShop.Program.ModelExtrasSubItem", "class_car_shop_1_1_program_1_1_model_extras_sub_item.html", null ],
      [ "CarShop.Program.ModelItem", "class_car_shop_1_1_program_1_1_model_item.html", null ],
      [ "CarShop.Program.ModelsSubItem", "class_car_shop_1_1_program_1_1_models_sub_item.html", null ],
      [ "CarShop.Program.NotCrudItem", "class_car_shop_1_1_program_1_1_not_crud_item.html", null ]
    ] ],
    [ "CarShop.Data.Model", "class_car_shop_1_1_data_1_1_model.html", null ],
    [ "CarShop.Data.ModelExtras", "class_car_shop_1_1_data_1_1_model_extras.html", null ],
    [ "CarShop.Logic.Tests.ModelExtrasLogicTest", "class_car_shop_1_1_logic_1_1_tests_1_1_model_extras_logic_test.html", null ],
    [ "CarShop.Logic.Tests.ModelLogicTest", "class_car_shop_1_1_logic_1_1_tests_1_1_model_logic_test.html", null ],
    [ "CarShop.JavaWeb.ModelsFromWeb", "class_car_shop_1_1_java_web_1_1_models_from_web.html", null ],
    [ "CarShop.Logic.Tests.NotCrudLogicTest", "class_car_shop_1_1_logic_1_1_tests_1_1_not_crud_logic_test.html", null ]
];