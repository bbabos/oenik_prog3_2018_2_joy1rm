var namespace_car_shop_1_1_repository =
[
    [ "BrandRepo", "class_car_shop_1_1_repository_1_1_brand_repo.html", "class_car_shop_1_1_repository_1_1_brand_repo" ],
    [ "ExtraRepo", "class_car_shop_1_1_repository_1_1_extra_repo.html", "class_car_shop_1_1_repository_1_1_extra_repo" ],
    [ "IRepository", "interface_car_shop_1_1_repository_1_1_i_repository.html", "interface_car_shop_1_1_repository_1_1_i_repository" ],
    [ "ModelExtrasRepo", "class_car_shop_1_1_repository_1_1_model_extras_repo.html", "class_car_shop_1_1_repository_1_1_model_extras_repo" ],
    [ "ModelRepo", "class_car_shop_1_1_repository_1_1_model_repo.html", "class_car_shop_1_1_repository_1_1_model_repo" ]
];