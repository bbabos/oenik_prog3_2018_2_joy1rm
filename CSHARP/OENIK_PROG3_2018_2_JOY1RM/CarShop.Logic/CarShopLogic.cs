﻿// <copyright file="CarShopLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CarShop.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using CarShop.Data;
    using CarShop.Repository;

    /// <summary>
    /// All the functions that can be called in the CarShop
    /// </summary>
    public class CarShopLogic : ILogic
    {
        /// <summary>
        /// Gets or sets the brand repository
        /// </summary>
        public IRepository<Brand> BrandRepo { get; set; }

        /// <summary>
        /// Gets or sets the model repository
        /// </summary>
        public IRepository<Model> ModelRepo { get; set; }

        /// <summary>
        /// Gets or sets the extra repository
        /// </summary>
        public IRepository<Extra> ExtraRepo { get; set; }

        /// <summary>
        /// Gets or sets the modelextras repository
        /// </summary>
        public IRepository<ModelExtras> ModelExtrasRepo { get; set; }

        /// <summary>
        /// Delete a brand
        /// </summary>
        /// <param name="brand">The brand that will be deleted</param>
        public void DeleteBrand(Brand brand)
        {
            this.BrandRepo.Delete(brand);
        }

        /// <summary>
        /// Delete an extra
        /// </summary>
        /// <param name="extra">The extra that will be deleted</param>
        public void DeleteExtra(Extra extra)
        {
            this.ExtraRepo.Delete(extra);
        }

        /// <summary>
        /// Delete a model
        /// </summary>
        /// <param name="model">The model that will be deleted</param>
        public void DeleteModel(Model model)
        {
            this.ModelRepo.Delete(model);
        }

        /// <summary>
        /// Delete a modelextra
        /// </summary>
        /// <param name="modelExtras">The modelextra that will be deleted</param>
        public void DeleteModelExtras(ModelExtras modelExtras)
        {
            this.ModelExtrasRepo.Delete(modelExtras);
        }

        /// <summary>
        /// Get all of the extras
        /// </summary>
        /// <returns>All of the extras</returns>
        public IEnumerable<Extra> GetAllExtras()
        {
            return this.ExtraRepo.GetAll();
        }

        /// <summary>
        /// Get all of the ModelExtras
        /// </summary>
        /// <returns>All of the ModelExtras</returns>
        public IEnumerable<ModelExtras> GetAllModelExtras()
        {
            return this.ModelExtrasRepo.GetAll();
        }

        /// <summary>
        /// Get all of the models
        /// </summary>
        /// <returns>All of the models</returns>
        public IEnumerable<Model> GetAllModels()
        {
            return this.ModelRepo.GetAll();
        }

        /// <summary>
        /// Get all of the brands
        /// </summary>
        /// <returns>All of the brands</returns>
        public IEnumerable<Brand> GetAllBrands()
        {
            return this.BrandRepo.GetAll();
        }

        /// <summary>
        /// Insert a new Extra
        /// </summary>
        /// <param name="extra">The new extra that will be added</param>
        public void InsertExtra(Extra extra)
        {
            this.ExtraRepo.Insert(extra);
        }

        /// <summary>
        /// Insert a new Model
        /// </summary>
        /// <param name="model">The new model that will be added</param>
        public void InsertModel(Model model)
        {
            this.ModelRepo.Insert(model);
        }

        /// <summary>
        /// Insert a new ModelExtras
        /// </summary>
        /// <param name="modelExtras">The new modelExtras that will be added</param>
        public void InsertModelExtras(ModelExtras modelExtras)
        {
            this.ModelExtrasRepo.Insert(modelExtras);
        }

        /// <summary>
        /// Insert a new Brand
        /// </summary>
        /// <param name="brand">The new brand that will be added</param>
        public void InsertBrand(Brand brand)
        {
            this.BrandRepo.Insert(brand);
        }

        /// <summary>
        /// Update a Brand
        /// </summary>
        /// <param name="brand">The brand that has been updated</param>
        public void UpdateBrand(Brand brand)
        {
            this.BrandRepo.Update(brand);
        }

        /// <summary>
        /// Update an Extra
        /// </summary>
        /// <param name="extra">The extra that has been updated</param>
        public void UpdateExtra(Extra extra)
        {
            this.ExtraRepo.Update(extra);
        }

        /// <summary>
        /// Update a Model
        /// </summary>
        /// <param name="model">The model that has been updated</param>
        public void UpdateModel(Model model)
        {
            this.ModelRepo.Update(model);
        }

        /// <summary>
        /// Update a ModelExtras
        /// </summary>
        /// <param name="modelExtras">The modelExtras that has been updated</param>
        public void UpdateModelExtras(ModelExtras modelExtras)
        {
            this.ModelExtrasRepo.Update(modelExtras);
        }

        /// <summary>
        /// Get the cars with full price
        /// </summary>
        /// <returns>Returns the cars with the base price + extras's price</returns>
        public IEnumerable<object> GetCarSumPrices()
        {
            return this.GetAllModels()
                .Select(x => new
                {
                    x.Brand.BrandName,
                    x.ModelName,
                    FullPrice = x.Price + x.ModelExtras.Sum(y => y.Extra.Price)
                });
        }

        /// <summary>
        /// Get the average base price for all brands
        /// </summary>
        /// <returns>Returns the average prices for brands</returns>
        public IEnumerable<object> GetAverageBrandPrice()
        {
            return this.GetAllBrands()
                .Select(x => new
                {
                    x.BrandName,
                    AveragePrice = x.Models.Average(y => y.Price)
                });
        }

        /// <summary>
        /// Get how many times an extra category were used
        /// </summary>
        /// <returns>Returns how many times an extra category were used</returns>
        public IEnumerable<object> GetNumberExtraCategoryUseage()
        {
            return this.GetAllExtras()
                .GroupBy(x => x.Category)
                .Select(x => new
                {
                    ExtraCategory = x.Key,
                    NumberOfUse = x.Sum(y => y.ModelExtras.Count())
                });
        }
    }
}
