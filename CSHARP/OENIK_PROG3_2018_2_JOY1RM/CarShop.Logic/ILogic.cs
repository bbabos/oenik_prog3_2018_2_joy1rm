﻿// <copyright file="ILogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CarShop.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using CarShop.Data;
    using CarShop.Repository;

    /// <summary>
    /// Interface for the logic
    /// </summary>
    public interface ILogic
    {
        /// <summary>
        /// Gets or sets the brand repository
        /// </summary>
        IRepository<Brand> BrandRepo { get; set; }

        /// <summary>
        /// Gets or sets the model repository
        /// </summary>
        IRepository<Model> ModelRepo { get; set; }

        /// <summary>
        /// Gets or sets the extra Repository repository
        /// </summary>
        IRepository<Extra> ExtraRepo { get; set; }

        /// <summary>
        /// Gets or sets the modelExtras repository repository
        /// </summary>
        IRepository<ModelExtras> ModelExtrasRepo { get; set; }

        /// <summary>
        /// Insert a new Brand
        /// </summary>
        /// <param name="brand">The new brand that will be added</param>
        void InsertBrand(Brand brand);

        /// <summary>
        /// Get all of the brands
        /// </summary>
        /// <returns>All of the brands</returns>
        IEnumerable<Brand> GetAllBrands();

        /// <summary>
        /// Update a Brand
        /// </summary>
        /// <param name="brand">The brand that has been updated</param>
        void UpdateBrand(Brand brand);

        /// <summary>
        /// Delete a Brand
        /// </summary>
        /// <param name="brand">The brand that will be deleted</param>
        void DeleteBrand(Brand brand);

        /// <summary>
        /// Delete an ModelExtras
        /// </summary>
        /// <param name="mODEL_EXTRAS">The modelExtras that will be deleted</param>
        void DeleteModelExtras(ModelExtras mODEL_EXTRAS);

        /// <summary>
        /// Update a ModelExtras
        /// </summary>
        /// <param name="mODEL_EXTRAS">The modelExtras that has been updated</param>
        void UpdateModelExtras(ModelExtras mODEL_EXTRAS);

        /// <summary>
        /// Insert a new ModelExtras
        /// </summary>
        /// <param name="mODEL_EXTRAS">The new modelExtras that will be added</param>
        void InsertModelExtras(ModelExtras mODEL_EXTRAS);

        /// <summary>
        /// Get all of the ModelExtras
        /// </summary>
        /// <returns>All of the ModelExtras</returns>
        IEnumerable<ModelExtras> GetAllModelExtras();

        /// <summary>
        /// Get all of the extras
        /// </summary>
        /// <returns>All of the extras</returns>
        IEnumerable<Extra> GetAllExtras();

        /// <summary>
        /// Insert a new Extra
        /// </summary>
        /// <param name="extra">The new extra that will be added</param>
        void InsertExtra(Extra extra);

        /// <summary>
        /// Update an Extra
        /// </summary>
        /// <param name="extra">The extra that has been updated</param>
        void DeleteExtra(Extra extra);

        /// <summary>
        /// Delete an Extra
        /// </summary>
        /// <param name="extra">The extra that will be deleted</param>
        void UpdateExtra(Extra extra);

        /// <summary>
        /// Get all of the models
        /// </summary>
        /// <returns>All of the models</returns>
        IEnumerable<Model> GetAllModels();

        /// <summary>
        /// Insert a new Model
        /// </summary>
        /// <param name="model">The new model that will be added</param>
        void InsertModel(Model model);

        /// <summary>
        /// Update a Model
        /// </summary>
        /// <param name="model">The model that has been updated</param>
        void UpdateModel(Model model);

        /// <summary>
        /// Delete a Model
        /// </summary>
        /// <param name="model">The model that will be deleted</param>
        void DeleteModel(Model model);

        /// <summary>
        /// Get the cars with full price
        /// </summary>
        /// <returns>Returns the cars with the base price + extras's price</returns>
        IEnumerable<object> GetCarSumPrices();

        /// <summary>
        /// Get the average base price for all brands
        /// </summary>
        /// <returns>Returns the average prices for brands</returns>
        IEnumerable<object> GetAverageBrandPrice();

        /// <summary>
        /// Get gow many times an extra category were used
        /// </summary>
        /// <returns>Returns how many times an extra category were used</returns>
        IEnumerable<object> GetNumberExtraCategoryUseage();
    }
}
