﻿// <copyright file="ModelsSubItem.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CarShop.Program
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using CarShop.Data;
    using CarShop.Logic;

    /// <summary>
    /// The page to add or edit a model entity
    /// </summary>
    public class ModelsSubItem : Item
    {
        private Model model;
        private bool isNew;

        /// <summary>
        /// Initializes a new instance of the <see cref="ModelsSubItem"/> class.
        /// </summary>
        /// <param name="title">The page title</param>
        /// <param name="logic">The logic that the page needs to use</param>
        public ModelsSubItem(string title, ILogic logic)
            : base(title, logic)
        {
            this.model = new Model();
            this.isNew = true;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ModelsSubItem"/> class.
        /// </summary>
        /// <param name="title">The page title</param>
        /// <param name="logic">The logic that the page needs to use</param>
        /// <param name="model">The model that will be modified</param>
        public ModelsSubItem(string title, ILogic logic, Model model)
            : base(title, logic)
        {
            this.model = model;
        }

        /// <summary>
        /// Render the page
        /// </summary>
        public override void Provider()
        {
            ConsoleKeyInfo selectedItem = default(ConsoleKeyInfo);

            Console.Clear();
            Console.WriteLine("*** MODELS ***");
            Console.WriteLine();

            Console.Write("Model name(" + this.model.ModelName + "): ");
            this.model.ModelName = Console.ReadLine();

            Console.Write("Brand ID(" + this.model.BrandId + "): ");
            this.model.BrandId = int.TryParse(Console.ReadLine(), out var temp) ? temp : (int?)null;

            Console.Write("Engine size(" + this.model.EngineSize + "): ");
            this.model.EngineSize = int.TryParse(Console.ReadLine(), out var temp2) ? temp2 : (int?)null;

            Console.Write("Horse power(" + this.model.HorsePower + "): ");
            this.model.HorsePower = int.TryParse(Console.ReadLine(), out var temp3) ? temp3 : (int?)null;

            Console.Write("Price(" + this.model.Price + "): ");
            this.model.Price = int.TryParse(Console.ReadLine(), out var temp4) ? temp4 : (int?)null;

            Console.Write("Release year(" + this.model.ReleaseYear + "): ");
            this.model.ReleaseYear = int.TryParse(Console.ReadLine(), out var temp5) ? temp5 : (int?)null;

            do
            {
                Console.Clear();
                Console.WriteLine("*** MODELS ***");

                this.PrintDetails(this.model);

                Console.WriteLine("0 - Cancel, 1 - Save");
                Console.Write("Choose an option: ");
                selectedItem = Console.ReadKey();

                switch (selectedItem.KeyChar)
                {
                    case '1':
                        try
                        {
                            if (this.isNew)
                            {
                                this.Logic.InsertModel(this.model);
                            }
                            else
                            {
                                this.Logic.UpdateModel(this.model);
                            }
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine("Save failed!", e.Message);
                            Console.ReadLine();
                        }

                        break;
                }
            }
            while (selectedItem.KeyChar != '0' && selectedItem.KeyChar != '1');
        }
    }
}
