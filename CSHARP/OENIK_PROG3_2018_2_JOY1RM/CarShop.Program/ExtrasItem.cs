﻿// <copyright file="ExtrasItem.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CarShop.Program
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using CarShop.Data;
    using CarShop.Logic;

    /// <summary>
    /// The page for the Extras
    /// </summary>
    public class ExtrasItem : Item
    {
        private IEnumerable<Extra> extras;
        private bool isSelection;

        /// <summary>
        /// Initializes a new instance of the <see cref="ExtrasItem"/> class.
        /// </summary>
        /// <param name="title">The page title</param>
        /// <param name="logic">The logic that the page needs to use</param>
        public ExtrasItem(string title, ILogic logic)
            : base(title, logic)
        {
            this.extras = logic.GetAllExtras();
        }

        /// <summary>
        /// Renders the page
        /// </summary>
        public override void Provider()
        {
            ConsoleKeyInfo selectedItem = default(ConsoleKeyInfo);
            do
            {
                Console.Clear();
                Console.WriteLine("*** EXTRAS ***");
                Console.WriteLine();

                this.ShowTable(this.extras);

                if (this.isSelection)
                {
                    Console.Write("Choose an ID: ");
                    int? id = int.TryParse(Console.ReadLine(), out var temp) ? temp : (int?)null;

                    if (id == null)
                    {
                        Console.WriteLine("Invalid number!");
                        Console.ReadLine();
                    }
                    else
                    {
                        var extra = this.extras.SingleOrDefault(x => x.Id == id);

                        if (extra == null)
                        {
                            Console.WriteLine("Invalid ID!");
                            Console.ReadLine();
                        }
                        else
                        {
                            ConsoleKeyInfo selectedSubItem = default(ConsoleKeyInfo);
                            do
                            {
                                Console.Clear();
                                Console.WriteLine("*** EXTRAS ***");

                                this.PrintDetails(extra);

                                Console.WriteLine("0 - Back, 1 - Edit, 2 - Delete");
                                Console.Write("Choose an option: ");
                                selectedSubItem = Console.ReadKey();

                                switch (selectedSubItem.KeyChar)
                                {
                                    case '1':
                                        new ExtrasSubItem("EDIT EXTRA", this.Logic, extra).Provider();
                                        break;
                                    case '2':
                                        try
                                        {
                                            this.Logic.DeleteExtra(extra);
                                        }
                                        catch (DbUpdateException)
                                        {
                                            Console.WriteLine("Failed to delete the record!");
                                            Console.ReadLine();
                                        }

                                        break;
                                }
                            }
                            while (selectedSubItem.KeyChar != '0' && selectedSubItem.KeyChar != '1' && selectedSubItem.KeyChar != '2');
                        }
                    }

                    this.isSelection = false;
                }
                else
                {
                    Console.WriteLine("0 - Back, 1 - Insert, 2 - Select record");
                    Console.Write("Choose an option: ");
                    selectedItem = Console.ReadKey();

                    switch (selectedItem.KeyChar)
                    {
                        case '1':
                            new ExtrasSubItem("NEW EXTRA", this.Logic).Provider();
                            break;
                        case '2':
                            this.isSelection = true;
                            break;
                    }
                }
            }
            while (selectedItem.KeyChar != '0');
        }
    }
}
