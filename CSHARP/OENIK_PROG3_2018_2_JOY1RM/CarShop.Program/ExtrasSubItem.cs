﻿// <copyright file="ExtrasSubItem.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CarShop.Program
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using CarShop.Data;
    using CarShop.Logic;

    /// <summary>
    /// The page to add or edit an extra entity
    /// </summary>
    public class ExtrasSubItem : Item
    {
        private Extra extra;
        private bool isNew;

        /// <summary>
        /// Initializes a new instance of the <see cref="ExtrasSubItem"/> class.
        /// </summary>
        /// <param name="title">The page title</param>
        /// <param name="logic">The logic that the page needs to use</param>
        public ExtrasSubItem(string title, ILogic logic)
            : base(title, logic)
        {
            this.extra = new Extra();
            this.isNew = true;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ExtrasSubItem"/> class.
        /// </summary>
        /// <param name="title">The page title</param>
        /// <param name="logic">The logic that the page needs to use</param>
        /// <param name="extra">The extra that will be modified</param>
        public ExtrasSubItem(string title, ILogic logic, Extra extra)
            : base(title, logic)
        {
            this.extra = extra;
        }

        /// <summary>
        /// Render the page
        /// </summary>
        public override void Provider()
        {
            ConsoleKeyInfo selectedItem = default(ConsoleKeyInfo);

            Console.Clear();
            Console.WriteLine("*** EXTRAS ***");
            Console.WriteLine();

            Console.Write("Category(" + this.extra.Category + "): ");
            this.extra.Category = Console.ReadLine();

            Console.Write("Extra name(" + this.extra.ExtraName + "): ");
            this.extra.ExtraName = Console.ReadLine();

            Console.Write("Multiple use(" + this.extra.MultipleUse + "): ");
            this.extra.MultipleUse = bool.TryParse(Console.ReadLine(), out var temp) ? temp : (bool?)null;

            Console.Write("Price(" + this.extra.Price + "): ");
            this.extra.Price = int.TryParse(Console.ReadLine(), out var temp2) ? temp2 : (int?)null;

            do
            {
                Console.Clear();
                Console.WriteLine("*** EXTRAS ***");

                this.PrintDetails(this.extra);

                Console.WriteLine("0 - Cancel, 1 - Save");
                Console.Write("Choose an option: ");
                selectedItem = Console.ReadKey();

                switch (selectedItem.KeyChar)
                {
                    case '1':
                        try
                        {
                            if (this.isNew)
                            {
                                this.Logic.InsertExtra(this.extra);
                            }
                            else
                            {
                                this.Logic.UpdateExtra(this.extra);
                            }
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine("Save failed", e.Message);
                            Console.ReadLine();
                        }

                        break;
                }
            }
            while (selectedItem.KeyChar != '0' && selectedItem.KeyChar != '1');
        }
    }
}
