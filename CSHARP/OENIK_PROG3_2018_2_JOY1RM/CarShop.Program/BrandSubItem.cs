﻿// <copyright file="BrandSubItem.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CarShop.Program
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using CarShop.Data;
    using CarShop.Logic;

    /// <summary>
    /// The page to add or edit a brand entity
    /// </summary>
    public class BrandSubItem : Item
    {
        private Brand brand;
        private bool isNew;

        /// <summary>
        /// Initializes a new instance of the <see cref="BrandSubItem"/> class.
        /// </summary>
        /// <param name="title">The page title</param>
        /// <param name="logic">The logic that the page needs to use</param>
        public BrandSubItem(string title, ILogic logic)
            : base(title, logic)
        {
            this.brand = new Brand();
            this.isNew = true;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="BrandSubItem"/> class.
        /// </summary>
        /// <param name="title">The page title</param>
        /// <param name="logic">The logic that the page needs to use</param>
        /// <param name="brand">The brand that will be modified</param>
        public BrandSubItem(string title, ILogic logic, Brand brand)
            : base(title, logic)
        {
            this.brand = brand;
        }

        /// <summary>
        /// Render the page
        /// </summary>
        public override void Provider()
        {
            ConsoleKeyInfo selectedItem = default(ConsoleKeyInfo);

            Console.Clear();
            Console.WriteLine("*** BRANDS ***");

            Console.Write("Brand name(" + this.brand.BrandName + "): ");
            this.brand.BrandName = Console.ReadLine();

            Console.Write("Country(" + this.brand.CountryName + "): ");
            this.brand.CountryName = Console.ReadLine();

            Console.Write("Website url(" + this.brand.WebUrl + "): ");
            this.brand.WebUrl = Console.ReadLine();

            Console.Write("Founded in(" + this.brand.FoundedIn + "): ");
            this.brand.FoundedIn = int.TryParse(Console.ReadLine(), out var temp) ? temp : (int?)null;

            Console.Write("Sales(" + this.brand.Sales + "): ");
            this.brand.Sales = int.TryParse(Console.ReadLine(), out var temp2) ? temp2 : (int?)null;

            do
            {
                Console.Clear();
                Console.WriteLine("*** BRANDS ***");

                this.PrintDetails(this.brand);

                Console.WriteLine("0 - Cancel, 1 - Save");
                Console.Write("Choose an option: ");
                selectedItem = Console.ReadKey();

                switch (selectedItem.KeyChar)
                {
                    case '1':
                        try
                        {
                            if (this.isNew)
                            {
                                this.Logic.InsertBrand(this.brand);
                            }
                            else
                            {
                                this.Logic.UpdateBrand(this.brand);
                            }
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine("Save failed", e.Message);
                            Console.ReadLine();
                        }

                        break;
                }
            }
            while (selectedItem.KeyChar != '0' && selectedItem.KeyChar != '1');
        }
    }
}
