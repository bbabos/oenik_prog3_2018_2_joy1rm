﻿// <copyright file="Item.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CarShop.Program
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using CarShop.Logic;

    /// <summary>
    /// A basic page
    /// </summary>
    public abstract class Item
    {
        private string title;

        /// <summary>
        /// Initializes a new instance of the <see cref="Item"/> class.
        /// </summary>
        /// <param name="title">The title of the page</param>
        /// <param name="logic">The logic that the page needs to use</param>
        protected Item(string title, ILogic logic)
        {
            this.title = title;
            this.Logic = logic;
        }

        /// <summary>
        /// Gets the logic
        /// </summary>
        public ILogic Logic { get; private set; }

        /// <summary>
        /// How to render the page
        /// </summary>
        public virtual void Provider()
        {
        }

        /// <summary>
        /// Makes the text center aligned
        /// </summary>
        /// <param name="text">The text that needs to be aligned</param>
        /// <param name="freeSpace">The available space</param>
        /// <returns>Center aligned text</returns>
        protected string ToCenter(string text, int freeSpace)
        {
            return (freeSpace > 1 ? new string(' ', freeSpace / 2) : " ") + text + (freeSpace > 1 ? new string(' ', freeSpace / 2) : " ");
        }

        /// <summary>
        /// Shows a table of the specified items
        /// </summary>
        /// <param name="db_items">The items to display</param>
        protected void ShowTable(IEnumerable<object> db_items)
        {
            foreach (var item in db_items)
            {
                var propertyInfos = item.GetType().GetProperties();

                propertyInfos = propertyInfos.Where(property => !property.GetGetMethod().IsVirtual).ToArray();
                int cellLength = (Console.WindowWidth / propertyInfos.Length) - 1;

                foreach (var property in propertyInfos)
                {
                    int freeSpace = cellLength - property.Name.Length;

                    Console.Write(this.ToCenter(property.Name, freeSpace));
                }

                Console.WriteLine();

                foreach (var prop in propertyInfos)
                {
                    int spaceLeft = cellLength - prop.GetValue(item).ToString().Length;

                    Console.Write(this.ToCenter(prop.GetValue(item).ToString(), spaceLeft));
                }

                Console.WriteLine("\n");
            }
        }

        /// <summary>
        /// Displays the details of the given entity
        /// </summary>
        /// <param name="entity">The entity to display</param>
        protected void PrintDetails(object entity)
        {
            var properties = entity.GetType().GetProperties();
            properties = properties.Where(property => !property.GetGetMethod().IsVirtual).ToArray();

            Console.WriteLine();
            foreach (var property in properties)
            {
                Console.WriteLine(property.Name + ": " + property.GetValue(entity));
            }

            Console.WriteLine();
        }
    }
}
