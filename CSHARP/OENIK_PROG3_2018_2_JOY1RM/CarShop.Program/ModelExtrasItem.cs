﻿// <copyright file="ModelExtrasItem.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CarShop.Program
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using CarShop.Data;
    using CarShop.Logic;

    /// <summary>
    /// The page for ModelExtras
    /// </summary>
    public class ModelExtrasItem : Item
    {
        private IEnumerable<ModelExtras> modelExtras;
        private bool isSelection;

        /// <summary>
        /// Initializes a new instance of the <see cref="ModelExtrasItem"/> class.
        /// </summary>
        /// <param name="title">The page title</param>
        /// <param name="logic">The logic that the page needs to use</param>
        public ModelExtrasItem(string title, ILogic logic)
            : base(title, logic)
        {
            this.modelExtras = logic.GetAllModelExtras();
        }

        /// <summary>
        /// Renders the page
        /// </summary>
        public override void Provider()
        {
            ConsoleKeyInfo selectedItem = default(ConsoleKeyInfo);
            do
            {
                Console.Clear();
                Console.WriteLine("*** MODEL EXTRAS ***");
                Console.WriteLine();

                this.ShowTable(this.modelExtras);

                if (this.isSelection)
                {
                    Console.WriteLine("Choose an ID: ");
                    int? id = int.TryParse(Console.ReadLine(), out var temp) ? temp : (int?)null;

                    if (id == null)
                    {
                        Console.WriteLine("Invalid number!");
                        Console.ReadLine();
                    }
                    else
                    {
                        var modelExtras = this.modelExtras.SingleOrDefault(x => x.Id == id);

                        if (modelExtras == null)
                        {
                            Console.WriteLine("Invalid ID!");
                            Console.ReadLine();
                        }
                        else
                        {
                            ConsoleKeyInfo selectedSubItem = default(ConsoleKeyInfo);
                            do
                            {
                                Console.Clear();
                                Console.WriteLine("*** MODEL EXTRAS ***");

                                this.PrintDetails(modelExtras);

                                Console.WriteLine("0 - Back, 1 - Edit, 2 - Delete");
                                Console.Write("Choose an option: ");
                                selectedSubItem = Console.ReadKey();

                                switch (selectedSubItem.KeyChar)
                                {
                                    case '1':
                                        new ModelExtrasSubItem("EDIT MODEL WITH EXTRA", this.Logic, modelExtras).Provider();
                                        break;
                                    case '2':
                                        try
                                        {
                                            this.Logic.DeleteModelExtras(modelExtras);
                                        }
                                        catch (DbUpdateException)
                                        {
                                            Console.WriteLine("Failed to delete a record!");
                                            Console.ReadLine();
                                        }

                                        break;
                                }
                            }
                            while (selectedSubItem.KeyChar != '0' && selectedSubItem.KeyChar != '1' && selectedSubItem.KeyChar != '2');
                        }
                    }

                    this.isSelection = false;
                }
                else
                {
                    Console.WriteLine("0 - Back, 1 - Insert, 2 - Select record");
                    Console.Write("Choose an option: ");
                    selectedItem = Console.ReadKey();

                    switch (selectedItem.KeyChar)
                    {
                        case '1':
                            new ModelExtrasSubItem("NEW MODEL WITH EXTRAS", this.Logic).Provider();
                            break;
                        case '2':
                            this.isSelection = true;
                            break;
                    }
                }
            }
            while (selectedItem.KeyChar != '0');
        }
    }
}
