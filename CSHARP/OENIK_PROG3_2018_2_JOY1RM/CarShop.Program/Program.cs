﻿namespace CarShop.Program
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using CarShop.Logic;

    public static class Program
    {
        private static ILogic logic = new CarShopLogic();

        private static void Menu()
        {
            Console.WriteLine("##########################");
            Console.WriteLine("| 0. EXIT                |");
            Console.WriteLine("| 1. Brands              |");
            Console.WriteLine("| 2. Models              |");
            Console.WriteLine("| 3. Extras              |");
            Console.WriteLine("| 4. Models with extras  |");
            Console.WriteLine("| 5. 1st not CRUD        |");
            Console.WriteLine("| 6. 2nd not CRUD        |");
            Console.WriteLine("| 7. 3rd not CRUD        |");
            Console.WriteLine("| 8. Java API            |");
            Console.WriteLine("##########################");
            Console.Write("Choose an option: ");

            ConsoleKeyInfo selectedmenu = Console.ReadKey();

            switch (selectedmenu.KeyChar)
            {
                case '0':
                    Environment.Exit(0);
                    break;
                case '1':
                    BrandsPage();
                    break;
                case '2':
                    ModelsPage();
                    break;
                case '3':
                    ExtrasPage();
                    break;
                case '4':
                    ModelExtrasPage();
                    break;
                case '5':
                    Console.WriteLine("Ez még nem működik");
                    break;
                case '6':
                    Console.WriteLine("Ez még nem működik");
                    break;
                case '7':
                    Console.WriteLine("Ez még nem működik");
                    break;
                case '8':
                    Console.WriteLine("Ez még nem működik");
                    break;
                default:
                    break;
            }
        }

        private static void BrandsPage()
        {
            ConsoleKeyInfo selectedItem;
            do
            {
                Console.Clear();
                Console.WriteLine("###########################");
                Console.WriteLine("|         BRANDS          |");
                Console.WriteLine("###########################");

                var brands = logic.GetAllBrands();
                ShowTable(brands);

                Console.WriteLine("| 0 - Exit | 1 - Insert | 2 - Select record |");
                Console.Write("Choose an option: ");
                selectedItem = ExitFromItem();
            } while (selectedItem.KeyChar != '0');
            Console.Clear();
            Menu();
        }

        private static void ModelsPage()
        {
            ConsoleKeyInfo selectedItem;
            do
            {
                Console.Clear();
                Console.WriteLine("###########################");
                Console.WriteLine("|         Models          |");
                Console.WriteLine("###########################");

                var models = logic.GetAllModels();

                ShowTable(models);

                Console.WriteLine("| 0 - Exit | 1 - Insert | 2 - Select record |");
                Console.Write("Choose an option: ");
                selectedItem = ExitFromItem();
            } while (selectedItem.KeyChar != '0');
            Console.Clear();
            Menu();
        }

        private static void ExtrasPage()
        {
            ConsoleKeyInfo selectedItem;
            do
            {
                Console.Clear();
                Console.WriteLine("###########################");
                Console.WriteLine("|         Extras          |");
                Console.WriteLine("###########################");

                var extras = logic.GetAllExtras();

                ShowTable(extras);

                Console.WriteLine("| 0 - Exit | 1 - Insert | 2 - Select record |");
                Console.Write("Choose an option: ");
                selectedItem = ExitFromItem();
            } while (selectedItem.KeyChar != '0');
            Console.Clear();
            Menu();
        }

        private static void ModelExtrasPage()
        {
            ConsoleKeyInfo selectedItem;
            do
            {
                Console.Clear();
                Console.WriteLine("#######################################");
                Console.WriteLine("|          Models with extras         |");
                Console.WriteLine("#######################################");

                var modelExtras = logic.GetAllModelExtras();

                ShowTable(modelExtras);

                Console.WriteLine("| 0 - Exit | 1 - Insert | 2 - Select record |");
                Console.Write("Choose an option: ");
                selectedItem = ExitFromItem();
            } while (selectedItem.KeyChar != '0');
            Console.Clear();
            Menu();
        }

        private static void ShowTable(IEnumerable<object> db_items)
        {
            foreach (var item in db_items)
            {
                var propertyInfos = item.GetType().GetProperties();

                propertyInfos = propertyInfos.Where(property => !property.GetGetMethod().IsVirtual).ToArray();
                int cellLength = (Console.WindowWidth / propertyInfos.Length) - 1;

                foreach (var property in propertyInfos)
                {
                    int freeSpace = cellLength - property.Name.Length;

                    Console.Write(CenterAlign(property.Name, freeSpace));
                }

                Console.WriteLine();

                foreach (var prop in propertyInfos)
                {
                    int spaceLeft = cellLength - prop.GetValue(item).ToString().Length;

                    Console.Write(CenterAlign(prop.GetValue(item).ToString(), spaceLeft));
                }

                Console.WriteLine("\n");
            }
        }

        private static string CenterAlign(string text, int spaceLeft)
        {
            return (spaceLeft > 1 ? new string(' ', spaceLeft / 2) : " ") + text + (spaceLeft > 1 ? new string(' ', spaceLeft / 2) : " ");
        }

        private static ConsoleKeyInfo ExitFromItem()
        {
            ConsoleKeyInfo selectedmenu = Console.ReadKey();
            return selectedmenu;
        }

        private static void Main(string[] args)
        {
            Menu();
        }
    }
}