﻿// <copyright file="NotCrudItem.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CarShop.Program
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using CarShop.Logic;

    /// <summary>
    /// A page to display the given items in a table
    /// </summary>
    public class NotCrudItem : Item
    {
        private IEnumerable<object> items;
        private string crudTitle;

        /// <summary>
        /// Initializes a new instance of the <see cref="NotCrudItem"/> class.
        /// </summary>
        /// <param name="title">The page title</param>
        /// <param name="logic">The logic that the page needs to use</param>
        /// <param name="items">The items to display</param>
        public NotCrudItem(string title, ILogic logic, IEnumerable<object> items)
            : base(title, logic)
        {
            this.items = items;
            this.crudTitle = title;
        }

        /// <summary>
        /// Render the page
        /// </summary>
        public override void Provider()
        {
            ConsoleKeyInfo selectedItem = default(ConsoleKeyInfo);
            do
            {
                Console.Clear();
                Console.WriteLine("*** " + this.crudTitle + " ***");
                Console.WriteLine();

                this.ShowTable(this.items);
                Console.WriteLine();

                Console.WriteLine("0 - Back");
                Console.Write("Choose an option: ");
                selectedItem = Console.ReadKey();
            }
            while (selectedItem.KeyChar != '0');
        }
    }
}
