﻿// <copyright file="ConsoleApp.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CarShop.Program
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using CarShop.Data;
    using CarShop.Logic;
    using CarShop.Repository;

    /// <summary>
    /// A simple menu, the entry point of the app
    /// </summary>
    public static class ConsoleApp
    {
        private static void Main()
        {
            ILogic logic = new CarShopLogic();
            logic.BrandRepo = new BrandRepo();
            logic.ModelRepo = new ModelRepo();
            logic.ExtraRepo = new ExtraRepo();
            logic.ModelExtrasRepo = new ModelExtrasRepo();

            Menu menuItem = new Menu("CAR SHOP", logic);
            menuItem.Provider();
        }
    }
}
