﻿// <copyright file="ModelExtrasSubItem.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CarShop.Program
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using CarShop.Data;
    using CarShop.Logic;

    /// <summary>
    /// The page to add or edit a ModelExtra entity
    /// </summary>
    public class ModelExtrasSubItem : Item
    {
        private ModelExtras modelExtras;
        private bool isNew;

        /// <summary>
        /// Initializes a new instance of the <see cref="ModelExtrasSubItem"/> class.
        /// </summary>
        /// <param name="title">The page title</param>
        /// <param name="logic">The logic that the page needs to use</param>
        public ModelExtrasSubItem(string title, ILogic logic)
            : base(title, logic)
        {
            this.modelExtras = new ModelExtras();
            this.isNew = true;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ModelExtrasSubItem"/> class.
        /// </summary>
        /// <param name="title">The page title</param>
        /// <param name="logic">The logic that the page needs to use</param>
        /// <param name="modelExtras">The modelExtras that will be modified</param>
        public ModelExtrasSubItem(string title, ILogic logic, ModelExtras modelExtras)
            : base(title, logic)
        {
            this.modelExtras = modelExtras;
        }

        /// <summary>
        /// Render the page
        /// </summary>
        public override void Provider()
        {
            ConsoleKeyInfo selectedItem = default(ConsoleKeyInfo);
            Console.Clear();

            Console.WriteLine("*** MODEL EXTRAS ***");
            Console.WriteLine();

            Console.Write("Extra ID(" + this.modelExtras.ExtraId + "): ");
            this.modelExtras.ExtraId = int.TryParse(Console.ReadLine(), out var temp) ? temp : (int?)null;

            Console.Write("Model ID(" + this.modelExtras.ModelId + "): ");
            this.modelExtras.ModelId = int.TryParse(Console.ReadLine(), out var temp2) ? temp2 : (int?)null;

            do
            {
                Console.Clear();
                Console.WriteLine("*** EXTRAS ***");

                this.PrintDetails(this.modelExtras);

                Console.WriteLine("0 - Cancel, 1 - Save");
                Console.Write("Choose an option: ");
                selectedItem = Console.ReadKey();

                switch (selectedItem.KeyChar)
                {
                    case '1':
                        try
                        {
                            if (this.isNew)
                            {
                                this.Logic.InsertModelExtras(this.modelExtras);
                            }
                            else
                            {
                                this.Logic.UpdateModelExtras(this.modelExtras);
                            }
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine("Save failed", e.Message);
                            Console.ReadLine();
                        }

                        break;
                }
            }
            while (selectedItem.KeyChar != '0' && selectedItem.KeyChar != '1');
        }
    }
}
