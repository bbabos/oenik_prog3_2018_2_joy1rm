﻿// <copyright file="Menu.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CarShop.Program
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using CarShop.Data;
    using CarShop.Logic;

    /// <summary>
    /// The main menu
    /// </summary>
    public class Menu : Item
    {
        /// <summary>
        /// Location URL of web end.
        /// </summary>
        public const string URL = "http://localhost:8080/CarShopWeb/";

        /// <summary>
        /// Initializes a new instance of the <see cref="Menu"/> class.
        /// </summary>
        /// <param name="title">The page title</param>
        /// <param name="logic">The logic that the page needs to use</param>
        public Menu(string title, ILogic logic)
            : base(title, logic)
        {
        }

        /// <summary>
        /// Renders the page
        /// </summary>
        public override void Provider()
        {
            ConsoleKeyInfo selectedItem;
            do
            {
                Console.Clear();

                Console.WriteLine("######################################");
                Console.WriteLine("| 0. EXIT                            |");
                Console.WriteLine("| 1. Brands table                    |");
                Console.WriteLine("| 2. Models table                    |");
                Console.WriteLine("| 3. Extras table                    |");
                Console.WriteLine("| 4. Models with extras table        |");
                Console.WriteLine("| 5. Cars with sum price             |");
                Console.WriteLine("| 6. Average prices for brands       |");
                Console.WriteLine("| 7. Extra category number of use    |");
                Console.WriteLine("| 8. Generate random models from web |");
                Console.WriteLine("######################################");
                Console.Write("Choose an option: ");

                selectedItem = Console.ReadKey();

                switch (selectedItem.KeyChar)
                {
                    case '1':
                        new BrandItem("BRANDS", this.Logic).Provider();
                        break;
                    case '2':
                        new ModelItem("MODELS", this.Logic).Provider();
                        break;
                    case '3':
                        new ExtrasItem("EXTRAS", this.Logic).Provider();
                        break;
                    case '4':
                        new ModelExtrasItem("MODELS WITH EXTRAS", this.Logic).Provider();
                        break;
                    case '5':
                        new NotCrudItem("CARS SUM PRICE", this.Logic, this.Logic.GetCarSumPrices()).Provider();
                        break;
                    case '6':
                        new NotCrudItem("AVERAGE PRICES FOR BRANDS", this.Logic, this.Logic.GetAverageBrandPrice()).Provider();
                        break;
                    case '7':
                        new NotCrudItem("EXTRA CATEGORY NUMBER OF USE", this.Logic, this.Logic.GetNumberExtraCategoryUseage()).Provider();
                        break;
                    case '8':
                        new JavaItem("GENERATE RANDOM MDOELS", this.Logic).Provider();
                        break;
                }
            }
            while (selectedItem.KeyChar != '0');
        }
    }
}
