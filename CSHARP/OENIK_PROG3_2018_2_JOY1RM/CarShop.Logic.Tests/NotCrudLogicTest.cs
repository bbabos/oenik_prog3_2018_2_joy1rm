﻿// <copyright file="NotCrudLogicTest.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CarShop.Logic.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using CarShop.Data;
    using CarShop.Repository;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// Test for all the complex logics
    /// </summary>
    [TestFixture]
    public class NotCrudLogicTest
    {
        private Mock<IRepository<Brand>> brandRepoMock = new Mock<IRepository<Brand>>();
        private Mock<IRepository<Extra>> extraRepoMock = new Mock<IRepository<Extra>>();
        private Mock<IRepository<Model>> modelsRepoMock = new Mock<IRepository<Model>>();
        private Mock<IRepository<ModelExtras>> modelExtrasRepoMock = new Mock<IRepository<ModelExtras>>();

        /// <summary>
        /// Setup the tests
        /// </summary>
        [SetUp]
        public void Setup()
        {
            Brand brand1 = new Brand()
            {
                Id = 1,
                BrandName = "Ford",
                CountryName = "USA",
                WebUrl = "www.ford.com",
                FoundedIn = 1903,
                Sales = 6600000
            };

            Brand brand2 = new Brand()
            {
                Id = 2,
                BrandName = "Audi",
                CountryName = "Germany",
                WebUrl = "www.audi.com",
                FoundedIn = 1969,
                Sales = 1879840
            };

            Brand brand3 = new Brand()
            {
                Id = 3,
                BrandName = "Volkswagen",
                CountryName = "Germany",
                WebUrl = "www.volkswagen.com",
                FoundedIn = 1937,
                Sales = 6073000
            };

            Extra extra1 = new Extra()
            {
                Id = 1,
                ExtraName = "JBL audio system",
                Category = "Entertainment",
                Price = 550,
                MultipleUse = true
            };

            Extra extra2 = new Extra()
            {
                Id = 2,
                ExtraName = "GPS Navigation",
                Category = "Electronics",
                Price = 210,
                MultipleUse = false
            };

            Extra extra3 = new Extra()
            {
                Id = 3,
                ExtraName = "Ford Sync",
                Category = "Entertainment",
                Price = 100,
                MultipleUse = false
            };

            ModelExtras modelExtras1 = new ModelExtras()
            {
                Id = 1,
                ModelId = 1,
                ExtraId = 3,
                Extra = extra3
            };

            ModelExtras modelExtras2 = new ModelExtras()
            {
                Id = 2,
                ModelId = 1,
                ExtraId = 2,
                Extra = extra2
            };

            ModelExtras modelExtras3 = new ModelExtras()
            {
                Id = 3,
                ModelId = 2,
                ExtraId = 1,
                Extra = extra1
            };

            extra1.ModelExtras = new[] { modelExtras3, modelExtras1 };
            extra2.ModelExtras = new[] { modelExtras2 };
            extra3.ModelExtras = new[] { modelExtras1 };

            Model model1 = new Model()
            {
                Id = 1,
                ModelName = "Focus",
                ReleaseYear = 2018,
                EngineSize = 1498,
                HorsePower = 175,
                Price = 17950,
                Brand = brand1,
                ModelExtras = new[] { modelExtras1, modelExtras2 }
            };

            Model model2 = new Model()
            {
                Id = 2,
                ModelName = "A4",
                ReleaseYear = 2015,
                EngineSize = 1984,
                HorsePower = 252,
                Price = 36000,
                Brand = brand2,
                ModelExtras = new[] { modelExtras3 }
            };

            Model model3 = new Model()
            {
                Id = 3,
                ModelName = "Golf",
                ReleaseYear = 2017,
                EngineSize = 1498,
                HorsePower = 150,
                Price = 20910,
                Brand = brand3
            };

            Model model4 = new Model()
            {
                Id = 4,
                ModelName = "Mustang",
                ReleaseYear = 2017,
                EngineSize = 1498,
                HorsePower = 150,
                Price = 40000,
                Brand = brand1
            };

            brand1.Models = new[] { model1, model4 };
            brand2.Models = new[] { model2 };
            brand3.Models = new[] { model3 };

            this.modelExtrasRepoMock
                .Setup(x => x.GetAll())
                .Returns(new[] { modelExtras1, modelExtras2, modelExtras3 });

            this.modelsRepoMock
                .Setup(x => x.GetAll())
                .Returns(new[] { model1, model2, model3, model4 });

            this.extraRepoMock
                .Setup(x => x.GetAll())
                .Returns(new[] { extra1, extra2, extra3 });

            this.brandRepoMock
                .Setup(x => x.GetAll())
                .Returns(new[] { brand1, brand2, brand3 });
        }

        /// <summary>
        /// Check if the cars sum price is the base price and the extra prices
        /// </summary>
        [Test]
        public void WhenGetCarsWithSumPrice_ReturnsTheCorrectNumbers()
        {
            ILogic logic = new CarShopLogic();
            logic.ModelRepo = this.modelsRepoMock.Object;

            var items = logic.GetCarSumPrices();

            Assert.That(items.Count(), Is.EqualTo(4));

            foreach (var item in items)
            {
                string modelName = item.GetType().GetProperty("ModelName").GetValue(item).ToString();
                int price = int.Parse(item.GetType().GetProperty("FullPrice").GetValue(item).ToString());

                if (modelName == "Focus")
                {
                    Assert.That(price, Is.EqualTo(18260));
                }
                else if (modelName == "A4")
                {
                    Assert.That(price, Is.EqualTo(36550));
                }
                else if (modelName == "Golf")
                {
                    Assert.That(price, Is.EqualTo(20910));
                }
                else if (modelName == "Mustang")
                {
                    Assert.That(price, Is.EqualTo(40000));
                }
            }
        }

        /// <summary>
        /// Check if the average price is the average of all the models of that brand
        /// </summary>
        [Test]
        public void WhenGetAveragePriceForBrands_ReturnsTheCorrectNumbers()
        {
            ILogic logic = new CarShopLogic();
            logic.BrandRepo = this.brandRepoMock.Object;

            var items = logic.GetAverageBrandPrice();

            Assert.That(items.Count(), Is.EqualTo(3));

            foreach (var item in items)
            {
                string brandName = item.GetType().GetProperty("BrandName").GetValue(item).ToString();
                int price = int.Parse(item.GetType().GetProperty("AveragePrice").GetValue(item).ToString());

                if (brandName == "Ford")
                {
                    Assert.That(price, Is.EqualTo(28975));
                }
                else if (brandName == "Audi")
                {
                    Assert.That(price, Is.EqualTo(36000));
                }
                else if (brandName == "Volkswagen")
                {
                    Assert.That(price, Is.EqualTo(20910));
                }
            }
        }

        /// <summary>
        /// Chech if the the categories number of use is correct
        /// </summary>
        [Test]
        public void WhenGetNumberExtraCategoryUseage_ReturnTheCorrectNumbers()
        {
            ILogic logic = new CarShopLogic();
            logic.ExtraRepo = this.extraRepoMock.Object;

            var items = logic.GetNumberExtraCategoryUseage();

            Assert.That(items.Count(), Is.EqualTo(2));

            foreach (var item in items)
            {
                string extraCategory = item.GetType().GetProperty("ExtraCategory").GetValue(item).ToString();
                int numOfUse = int.Parse(item.GetType().GetProperty("NumberOfUse").GetValue(item).ToString());

                if (extraCategory == "Entertainment")
                {
                    Assert.That(numOfUse, Is.EqualTo(3));
                }
                else if (extraCategory == "Electronics")
                {
                    Assert.That(numOfUse, Is.EqualTo(1));
                }
            }
        }
    }
}
