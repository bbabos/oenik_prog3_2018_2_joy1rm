﻿// <copyright file="ModelExtrasLogicTest.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CarShop.Logic.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using CarShop.Data;
    using CarShop.Repository;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// Tests for the ModelExtras logics in CarShopLogic
    /// </summary>
    [TestFixture]
    public class ModelExtrasLogicTest
    {
        private Mock<IRepository<ModelExtras>> repoMock = new Mock<IRepository<ModelExtras>>();

        /// <summary>
        /// Setup the tests
        /// </summary>
        [SetUp]
        public void Setup()
        {
            ModelExtras modelExtras1 = new ModelExtras()
            {
                Id = 1,
                ModelId = 1,
                ExtraId = 3
            };

            ModelExtras modelExtras2 = new ModelExtras()
            {
                Id = 2,
                ModelId = 1,
                ExtraId = 2
            };

            ModelExtras modelExtras3 = new ModelExtras()
            {
                Id = 3,
                ModelId = 2,
                ExtraId = 1
            };

            this.repoMock
                .Setup(x => x.GetAll())
                .Returns(new[] { modelExtras1, modelExtras2, modelExtras3 });
        }

        /// <summary>
        /// Test for returning all 3 modelExtrass
        /// </summary>
        [Test]
        public void WhenGetAllTheModelExtrass_ReturnAllTheThreeModelExtrass()
        {
            ILogic logic = new CarShopLogic();
            logic.ModelExtrasRepo = this.repoMock.Object;

            var modelExtrass = logic.GetAllModelExtras();

            Assert.That(modelExtrass.Count(), Is.EqualTo(3));
        }

        /// <summary>
        /// Testing if Insert is called during insertion
        /// </summary>
        [Test]
        public void WhenInsertAModelExtras_TheInsertFunctionIsCalled()
        {
            ModelExtras modelExtras = new ModelExtras();

            ILogic logic = new CarShopLogic();
            logic.ModelExtrasRepo = this.repoMock.Object;

            logic.InsertModelExtras(modelExtras);

            this.repoMock.Verify(x => x.Insert(It.IsAny<ModelExtras>()), Times.Once);
        }

        /// <summary>
        /// Testing if delete is called during deletion
        /// </summary>
        [Test]
        public void WhenDeleteAModelExtras_TheDeleteFunctionIsCalled()
        {
            ILogic logic = new CarShopLogic();
            logic.ModelExtrasRepo = this.repoMock.Object;

            var modelExtras = logic.GetAllModelExtras().First();
            logic.DeleteModelExtras(modelExtras);

            this.repoMock.Verify(x => x.Delete(It.IsAny<ModelExtras>()), Times.Once);
        }

        /// <summary>
        /// Testing if update method is called during update
        /// </summary>
        [Test]
        public void WhenUpdateAModelExtras_TheUpdateFunctionIsCalled()
        {
            ILogic logic = new CarShopLogic();
            logic.ModelExtrasRepo = this.repoMock.Object;

            var modelExtras = logic.GetAllModelExtras().First();
            modelExtras.ModelId = 3;
            logic.UpdateModelExtras(modelExtras);

            this.repoMock.Verify(x => x.Update(It.IsAny<ModelExtras>()), Times.Once);
        }
    }
}
