﻿// <copyright file="BrandLogicTest.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CarShop.Logic.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using CarShop.Data;
    using CarShop.Repository;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// Tests for the Brand logics in CarShopLogic
    /// </summary>
    [TestFixture]
    public class BrandLogicTest
    {
        private Mock<IRepository<Brand>> repoMock = new Mock<IRepository<Brand>>();

        /// <summary>
        /// Setup the tests
        /// </summary>
        [SetUp]
        public void Setup()
        {
            Brand brand1 = new Brand()
            {
                Id = 1,
                BrandName = "Ford",
                CountryName = "USA",
                WebUrl = "www.ford.com",
                FoundedIn = 1903,
                Sales = 6600000
            };

            Brand brand2 = new Brand()
            {
                Id = 2,
                BrandName = "Audi",
                CountryName = "Germany",
                WebUrl = "www.audi.com",
                FoundedIn = 1969,
                Sales = 1879840
            };

            Brand brand3 = new Brand()
            {
                Id = 3,
                BrandName = "Volkswagen",
                CountryName = "Germany",
                WebUrl = "www.volkswagen.com",
                FoundedIn = 1937,
                Sales = 6073000
            };

            this.repoMock
                .Setup(x => x.GetAll())
                .Returns(new[] { brand1, brand2, brand3 });
        }

        /// <summary>
        /// Test for returning all 3 brands
        /// </summary>
        [Test]
        public void WhenGetAllTheBrands_ReturnAllTheThreeBrands()
        {
            ILogic logic = new CarShopLogic();
            logic.BrandRepo = this.repoMock.Object;

            var brands = logic.GetAllBrands();

            Assert.That(brands.Count(), Is.EqualTo(3));
        }

        /// <summary>
        /// Testing if Insert is called during insertion
        /// </summary>
        [Test]
        public void WhenInsertABrand_TheInsertFunctionIsCalled()
        {
            Brand brand = new Brand();

            ILogic logic = new CarShopLogic();
            logic.BrandRepo = this.repoMock.Object;

            logic.InsertBrand(brand);

            this.repoMock.Verify(x => x.Insert(It.IsAny<Brand>()), Times.Once);
        }

        /// <summary>
        /// Testing if Insert is called during insertion
        /// </summary>
        [Test]
        public void WhenDeleteABrand_TheDeleteFunctionIsCalled()
        {
            ILogic logic = new CarShopLogic();
            logic.BrandRepo = this.repoMock.Object;

            var brand = logic.GetAllBrands().First();
            logic.DeleteBrand(brand);

            this.repoMock.Verify(x => x.Delete(It.IsAny<Brand>()), Times.Once);
        }

        /// <summary>
        /// Testing if delete is called during deletion
        /// </summary>
        [Test]
        public void WhenUpdateABrand_TheUpdateFunctionIsCalled()
        {
            ILogic logic = new CarShopLogic();
            logic.BrandRepo = this.repoMock.Object;

            var brand = logic.GetAllBrands().First();
            brand.BrandName = "Nissan";
            logic.UpdateBrand(brand);

            this.repoMock.Verify(x => x.Update(It.IsAny<Brand>()), Times.Once);
        }
    }
}
