﻿// <copyright file="ExtraLogicTest.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CarShop.Logic.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using CarShop.Data;
    using CarShop.Repository;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// Tests for the Extra related logics in CarShopLogic
    /// </summary>
    [TestFixture]
    public class ExtraLogicTest
    {
        private Mock<IRepository<Extra>> repoMock = new Mock<IRepository<Extra>>();

        /// <summary>
        /// Setup the tests
        /// </summary>
        [SetUp]
        public void Setup()
        {
            Extra extra1 = new Extra()
            {
                Id = 1,
                ExtraName = "JBL audio system",
                Category = "Entertainment",
                Price = 550,
                MultipleUse = true
            };

            Extra extra2 = new Extra()
            {
                Id = 2,
                ExtraName = "GPS Navigation",
                Category = "Electronics",
                Price = 210,
                MultipleUse = false
            };

            Extra extra3 = new Extra()
            {
                Id = 3,
                ExtraName = "Ford Sync",
                Category = "Entertainment",
                Price = 100,
                MultipleUse = false
            };

            this.repoMock
                .Setup(x => x.GetAll())
                .Returns(new[] { extra1, extra2, extra3 });
        }

        /// <summary>
        /// Test for returning all 3 extras
        /// </summary>
        [Test]
        public void WhenGetllTheExtras_ReturnAllTheThreeExtras()
        {
            ILogic logic = new CarShopLogic();
            logic.ExtraRepo = this.repoMock.Object;

            var extras = logic.GetAllExtras();

            Assert.That(extras.Count(), Is.EqualTo(3));
        }

        /// <summary>
        /// Testing if Insert is called during insertion
        /// </summary>
        [Test]
        public void WhenInsertAnExtra_TheInsertFunctionIsCalled()
        {
            Extra extra = new Extra();

            ILogic logic = new CarShopLogic();
            logic.ExtraRepo = this.repoMock.Object;

            logic.InsertExtra(extra);

            this.repoMock.Verify(x => x.Insert(It.IsAny<Extra>()), Times.Once);
        }

        /// <summary>
        /// Testing if delete is called during deletion
        /// </summary>
        [Test]
        public void WhenDeleteAnExtra_TheDeleteFunctionIsCalled()
        {
            ILogic logic = new CarShopLogic();
            logic.ExtraRepo = this.repoMock.Object;

            var extra = logic.GetAllExtras().First();
            logic.DeleteExtra(extra);

            this.repoMock.Verify(x => x.Delete(It.IsAny<Extra>()), Times.Once);
        }

        /// <summary>
        /// Testing if update method is called during update
        /// </summary>
        [Test]
        public void WhenUpdateAnExtra_TheUpdateFunctionIsCalled()
        {
            ILogic logic = new CarShopLogic();
            logic.ExtraRepo = this.repoMock.Object;

            var extra = logic.GetAllExtras().First();
            extra.ExtraName = "Assistance systems";
            logic.UpdateExtra(extra);

            this.repoMock.Verify(x => x.Update(It.IsAny<Extra>()), Times.Once);
        }
    }
}
