﻿// <copyright file="ModelLogicTest.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CarShop.Logic.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using CarShop.Data;
    using CarShop.Repository;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// Tests for the Model related logics in CarShopLogic
    /// </summary>
    [TestFixture]
    public class ModelLogicTest
    {
        private Mock<IRepository<Model>> repoMock = new Mock<IRepository<Model>>();

        /// <summary>
        /// Setup the tests
        /// </summary>
        [SetUp]
        public void Setup()
        {
            Model model1 = new Model()
            {
                Id = 1,
                ModelName = "Focus",
                ReleaseYear = 2018,
                EngineSize = 1498,
                HorsePower = 175,
                Price = 17950
            };

            Model model2 = new Model()
            {
                Id = 2,
                ModelName = "A4",
                ReleaseYear = 2015,
                EngineSize = 1984,
                HorsePower = 252,
                Price = 36000
            };

            Model model3 = new Model()
            {
                Id = 3,
                ModelName = "Golf",
                ReleaseYear = 2017,
                EngineSize = 1498,
                HorsePower = 150,
                Price = 20910
            };

            this.repoMock
                .Setup(x => x.GetAll())
                .Returns(new[] { model1, model2, model3 });
        }

        /// <summary>
        /// Test for returning all 3 models
        /// </summary>
        [Test]
        public void WhenGetAllTheModels_ReturnAllTheThreeModels()
        {
            ILogic logic = new CarShopLogic();
            logic.ModelRepo = this.repoMock.Object;

            var models = logic.GetAllModels();

            Assert.That(models.Count(), Is.EqualTo(3));
        }

        /// <summary>
        /// Testing if Insert is called during insertion
        /// </summary>
        [Test]
        public void WhenInsertAModel_TheInsertFunctionIsCalled()
        {
            Model model = new Model();

            ILogic logic = new CarShopLogic();
            logic.ModelRepo = this.repoMock.Object;

            logic.InsertModel(model);

            this.repoMock.Verify(x => x.Insert(It.IsAny<Model>()), Times.Once);
        }

        /// <summary>
        /// Testing if delete is called during deletion
        /// </summary>
        [Test]
        public void WhenDeleteAModel_TheDeleteFunctionIsCalled()
        {
            ILogic logic = new CarShopLogic();
            logic.ModelRepo = this.repoMock.Object;

            var model = logic.GetAllModels().First();
            logic.DeleteModel(model);

            this.repoMock.Verify(x => x.Delete(It.IsAny<Model>()), Times.Once);
        }

        /// <summary>
        /// Testing if update method is called during update
        /// </summary>
        [Test]
        public void WhenUpdateAModel_TheUpdateFunctionIsCalled()
        {
            ILogic logic = new CarShopLogic();
            logic.ModelRepo = this.repoMock.Object;

            var model = logic.GetAllModels().First();
            model.ModelName = "Prius";
            logic.UpdateModel(model);

            this.repoMock.Verify(x => x.Update(It.IsAny<Model>()), Times.Once);
        }
    }
}
