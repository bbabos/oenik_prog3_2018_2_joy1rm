﻿// <copyright file="BrandRepo.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CarShop.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using CarShop.Data;

    /// <summary>
    /// Repository for accessing the Brands
    /// </summary>
    public class BrandRepo : IRepository<Brand>
    {
        private CarShopDBEntities entities = new CarShopDBEntities();

        /// <summary>
        /// Insert a new Brand
        /// </summary>
        /// <param name="brand">The brand that will be added</param>
        public void Insert(Brand brand)
        {
            this.entities.Brands.Add(brand);
            this.entities.SaveChanges();
        }

        /// <summary>
        /// Getting all the Brands
        /// </summary>
        /// <returns>All the Brands</returns>
        public IEnumerable<Brand> GetAll()
        {
            return this.entities.Brands;
        }

        /// <summary>
        /// Updating an existing Brand
        /// </summary>
        /// <param name="brand">The brand that has been updated</param>
        public void Update(Brand brand)
        {
            this.entities.Brands.Attach(brand);
            this.entities.SaveChanges();
        }

        /// <summary>
        /// Deleting an existing Brand
        /// </summary>
        /// <param name="brand">The brand that will be deleted</param>
        public void Delete(Brand brand)
        {
            this.entities.Brands.Remove(brand);
            this.entities.SaveChanges();
        }
    }
}
