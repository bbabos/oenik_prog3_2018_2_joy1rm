﻿// <copyright file="IRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CarShop.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Interface for the repositories
    /// </summary>
    /// <typeparam name="T">An entity type</typeparam>
    public interface IRepository<T>
    {
        /// <summary>
        /// Insert a new entity
        /// </summary>
        /// <param name="entity">The entity that will be added</param>
        void Insert(T entity);

        /// <summary>
        /// Getting all the entities
        /// </summary>
        /// <returns>All the entities</returns>
        IEnumerable<T> GetAll();

        /// <summary>
        /// Updating an existing entity
        /// </summary>
        /// <param name="entity">The entity that has been updated</param>
        void Update(T entity);

        /// <summary>
        /// Deleting an existing entity
        /// </summary>
        /// <param name="entity">The entity that will be deleted</param>
        void Delete(T entity);
    }
}
