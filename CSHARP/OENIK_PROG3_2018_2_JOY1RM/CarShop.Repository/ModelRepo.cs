﻿// <copyright file="ModelRepo.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CarShop.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using CarShop.Data;

    /// <summary>
    /// Repository for accessing the Models
    /// </summary>
    public class ModelRepo : IRepository<Model>
    {
        private CarShopDBEntities entities = new CarShopDBEntities();

        /// <summary>
        /// Insert a new Model
        /// </summary>
        /// <param name="model">The model that will be added</param>
        public void Insert(Model model)
        {
            this.entities.Models.Add(model);
            this.entities.SaveChanges();
        }

        /// <summary>
        /// Getting all the Models
        /// </summary>
        /// <returns>All the Models</returns>
        public IEnumerable<Model> GetAll()
        {
            return this.entities.Models;
        }

        /// <summary>
        /// Updating an existing Model
        /// </summary>
        /// <param name="model">The model to update</param>
        public void Update(Model model)
        {
            this.entities.Models.Attach(model);
            this.entities.SaveChanges();
        }

        /// <summary>
        /// Deleting an existing Model
        /// </summary>
        /// <param name="model">The model that will be deleted</param>
        public void Delete(Model model)
        {
            this.entities.Models.Remove(model);
            this.entities.SaveChanges();
        }
    }
}
