﻿// <copyright file="ExtraRepo.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CarShop.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using CarShop.Data;

    /// <summary>
    /// Repository for accessing the Extras
    /// </summary>
    public class ExtraRepo : IRepository<Extra>
    {
        private CarShopDBEntities entities = new CarShopDBEntities();

        /// <summary>
        /// Insert a new Extra
        /// </summary>
        /// <param name="extra">The extra that will be added</param>
        public void Insert(Extra extra)
        {
            this.entities.Extras.Add(extra);
            this.entities.SaveChanges();
        }

        /// <summary>
        /// Getting all the Extras
        /// </summary>
        /// <returns>All the Extras</returns>
        public IEnumerable<Extra> GetAll()
        {
            return this.entities.Extras;
        }

        /// <summary>
        /// Updating an existing Extra
        /// </summary>
        /// <param name="extra">The extra to update</param>
        public void Update(Extra extra)
        {
            this.entities.Extras.Attach(extra);
            this.entities.SaveChanges();
        }

        /// <summary>
        /// Deleting an existing Extra
        /// </summary>
        /// <param name="extra">The extra that will be deleted</param>
        public void Delete(Extra extra)
        {
            this.entities.Extras.Remove(extra);
            this.entities.SaveChanges();
        }
    }
}
