﻿// <copyright file="ModelExtrasRepo.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CarShop.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using CarShop.Data;

    /// <summary>
    /// Repository for accessing the ModelExtras
    /// </summary>
    public class ModelExtrasRepo : IRepository<ModelExtras>
    {
        private CarShopDBEntities entities = new CarShopDBEntities();

        /// <summary>
        /// Insert a new ModelExtras
        /// </summary>
        /// <param name="modelExtras">The ModelExtras that will be added</param>
        public void Insert(ModelExtras modelExtras)
        {
            this.entities.ModelExtras.Add(modelExtras);
            this.entities.SaveChanges();
        }

        /// <summary>
        /// Getting all the ModelExtras
        /// </summary>
        /// <returns>All the ModelExtras</returns>
        public IEnumerable<ModelExtras> GetAll()
        {
            return this.entities.ModelExtras;
        }

        /// <summary>
        /// Updating an existing ModelExtras
        /// </summary>
        /// <param name="modelExtras">The ModelExtras to update</param>
        public void Update(ModelExtras modelExtras)
        {
            this.entities.ModelExtras.Attach(modelExtras);
            this.entities.SaveChanges();
        }

        /// <summary>
        /// Deleting an existing ModelExtras
        /// </summary>
        /// <param name="modelExtras">The ModelExtras that will be deleted</param>
        public void Delete(ModelExtras modelExtras)
        {
            this.entities.ModelExtras.Remove(modelExtras);
            this.entities.SaveChanges();
        }
    }
}
