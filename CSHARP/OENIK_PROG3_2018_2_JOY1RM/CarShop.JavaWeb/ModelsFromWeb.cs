﻿// <copyright file="ModelsFromWeb.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CarShop.JavaWeb
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Class to represent models generated by server.
    /// </summary>
    public class ModelsFromWeb
    {
        /// <summary>
        /// Gets or sets data.
        /// </summary>
        public string Release_year { get; set; }

        /// <summary>
        /// Gets or sets data.
        /// </summary>
        public string Engine_size { get; set; }

        /// <summary>
        /// Gets or sets data.
        /// </summary>
        public string Horse_power { get; set; }

        /// <summary>
        /// Gets or sets data.
        /// </summary>
        public string Price { get; set; }
    }
}
