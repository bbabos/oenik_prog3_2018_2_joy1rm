<%-- 
    Document   : index
    Created on : Nov 30, 2018, 2:24:53 PM
    Author     : BabósBenceÁdám
--%>

<%@page import="DB.RandomData"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Data generator for CarShop</title>
    </head>
    <body>
        <%if (request.getParameter("release_year") == null || request.getParameter("engine_size") == null || request.getParameter("horse_power") == null || request.getParameter("price") == null) {%>
            <%out.println("Parameter error!");%>
        <%} else {%>
                <% try {
                    String release = request.getParameter("release_year");
                    if (!release.contains("-")) {
                        out.println("Format error! Correct format: X-Y");
                        return;
                    }
                    String engine = request.getParameter("engine_size");
                    if (!engine.contains("-")) {
                        out.println("Format error! Correct format: X-Y");
                        return;
                    }
                    String hp = request.getParameter("horse_power");
                    if (!hp.contains("-")) {
                        out.println("Format error! Correct format: X-Y");
                        return;
                    }
                    String price = request.getParameter("price");
                    if (!price.contains("-")) {
                        out.println("Format error! Correct format: X-Y");
                        return;
                    }
                    out.println(RandomData.createJson(release, engine, hp, price));
                }
                catch (Exception e) {
                    out.println("Hiba! " + e);
                }%>
        <%}%>
    </body>
</html>
