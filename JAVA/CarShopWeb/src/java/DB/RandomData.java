package DB;

import java.io.IOException;
import java.util.Random;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author BabósBenceÁdám
 */
public class RandomData extends HttpServlet{
    private static Random rand = new Random();
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
    }
    
    public static int Generator(String dataRec) {
        int min = Integer.parseInt(dataRec.split("-")[0]);
        int max = Integer.parseInt(dataRec.split("-")[1]);
        return rand.nextInt((max - min) + 1) + min;
    }
    public static String createJson(String releaseyear, String enginesize, String horsepower, String price) 
    {
        JsonObjectBuilder model = Json.createObjectBuilder();
        model.add("RELEASE_YEAR", Generator(releaseyear));
        model.add("ENGINE_SIZE", Generator(enginesize));
        model.add("HORSE_POWER", Generator(horsepower));
        model.add("PRICE", Generator(price));
        JsonObject  done = model.build();
        return done.toString();
    }
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");
        processRequest(request, response);
    }
    
    @Override
    public String getServletInfo() {
        return "Short description";
    }
}
